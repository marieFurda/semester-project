const chance = require('chance');
const chanceobj = new chance();

const fs = require('fs') 

const generatePerson = () => {
    return {
        "employeeNum" : chanceobj.integer({min:23456, max: 34567}),
        "ssn" : chanceobj.ssn(),
        "birthday" : chanceobj.birthday(),
        "name" : chanceobj.name(),
        "gender" : chanceobj.gender(),
        "hireDate" : chanceobj.date(),
        "address" : chanceobj.address(),
        "city" : chanceobj.city(),
        "state" : chanceobj.state(),
        "zip" : chanceobj.zip(),
        "homePhone" : chanceobj.phone(),
        "cellPhone" : chanceobj.phone(),
        "email" : chanceobj.email(),
        "salary" : chanceobj.dollar({min: 30000, max: 120000}),
        "department" : chanceobj.profession({rank:true}),
        "departmentNum" : chanceobj.integer({min:1, max:6}),


    };
};

const people = Array.from({length :10000}, generatePerson);
//const dom = generatePerson();

//fs.writeFile('EmployeeData.txt', people, (err) => { 
      
    // In case of a error throw err. 
 //   if (err) throw err; 
//});

require('fs').writeFile(

    './employeeInformation1.txt',

    JSON.stringify(people),

    function (err) {
        if (err) {
            console.error('Crap happens');
        }
    }
);

console.log(people);
