const express = require('express');
const mysql = require('mysql');

// creating a connection 
const db = mysql.createConnection({
    host: "192.168.64.2",
    user: "marie",
    password: "sterlingspencer",
    database: 'employeeRecords'
})

//connect to MySQL
db.connect(err => {
    if(err){
        throw err;
    }
    console.log('MySQL Connected');
})

const app = express();

//This is where I am creatin the Database in xampp
app.get('/createdb', (req, res) => {
    let sql = 'CREATE DATABASE employeeRecords';
    db.query(sql, err => {
        if (err) {
            throw err;
        }
        res.send("Database created");
    });
});

//so we are using the index.js env within a package on node.js to create and control everything we want to see in node

//now im writing this query in order to create the first employee table in the employeeRecords database 
app.get('/createemployee', (req, res) => {
    let sql = 'CREATE TABLE employee(id int AUTO_INCREMENT, ssn VARCHAR(45), birth_date DATETIME, name VARCHAR(50), gender VARCHAR(8), hire_date DATETIME, address VARCHAR(45), city VARCHAR(45), state VARCHAR(45), zip_code VARCHAR(10), home_phone VARCHAR(12), cell_phone VARCHAR(12), email VARCHAR(25), PRIMARY KEY(id))'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Employee table created')
    })

})

//now I am going to create the salary table 
app.get('/createemployeeSalary', (req, res) => {
    let sql = 'CREATE TABLE salary(salary VARCHAR(15), from_date DATETIME, to_date DATETIME, PRIMARY KEY(from_date))'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Employee Salary table created')
    })

})

//now I am going to create the dept_emp table 
app.get('/createdeptEmp', (req, res) => {
    let sql = 'CREATE TABLE deptEmp(dept_no CHAR(4), from_date DATETIME, to_date DATETIME)'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Department Employee table created')
    })

})

//now I am going to create the department table 
app.get('/createDepartment', (req, res) => {
    let sql = 'CREATE TABLE department(dept_no CHAR(4), dept_name VARCHAR(40), PRIMARY KEY(dept_no))'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Department table created')
    })

})

//now I am going to create the department manager table 
app.get('/createDepartmentManager', (req, res) => {
    let sql = 'CREATE TABLE departmentManager(id int, dept_no int, from_date DATETIME, to_date DATETIME)'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Department Manager table created')
    })

})

//now I am going to create the employee title table 
app.get('/createemployeeTitle', (req, res) => {
    let sql = 'CREATE TABLE employeeTitle(id int, title VARCHAR(50), from_date DATETIME, to_date DATETIME)'
    db.query(sql, err =>{
        if(err){
            throw err
        }
        res.send('Employee title table created')
    })

})

//now I am going to try to update employee record information 
app.get('/updateemployee/:id', (req, res) => {
    let newName = 'Whatever the new name is'
    let sql = `UPDATE employee SET name = ' ${newName} 'WHERE id = ${req.params.id}`
    let query =db.query(sql, err => {
        if(err){
            throw err
        }
        res.send('Employee updated')
    })
})
//so with this, if you navigate to the "http://localhost:3000/updateemployee/" followed by the employee number, you will be able to update the employee
//I want to create a button click for this functionality and then also put in a text bar to better let someone edit the entire employee record

//Removing an employee 
app.get('/deleteemployee/:id', (req, res) => {
    let sql = `DELETE FROM employee WHERE id = ${req.params.id}`
    let query = db.query(sql, err => {
        if(err){
            throw err
        }
        res.send('Employee deleted')
    })
})
//so with this, if you navigate to the "http://localhost:3000/deleteemployee/" followed by the employee number, you will be able to delete the employee
//I want to put this functionality on a button click in the web application html

app.listen('3000', () => {
    console.log('Server Started on port 3000');
})