const chance = require('chance');
const chanceobj = new chance();

const fs = require('fs') 

const generateSalary = () => {
    return {
   
        "salary" : chanceobj.dollar({min: 30000, max: 120000}),
	    "from_date" : chanceobj.date(),
	    "to_date" : chanceobj.date()


    };
};

const salary = Array.from({length :10000}, generateSalary);


require('fs').writeFile(

    './salaryTableInformation1.txt',

    JSON.stringify(salary),

    function (err) {
        if (err) {
            console.error('There is something wrong with this');
        }
    }
);

console.log(salary);
