const chance = require('chance');
const chanceobj = new chance();

const fs = require('fs') 

const generatePerson = () => {
    return {
        //employee_no is generated and incremented in phpMyAdmin and MySql
        "ssn" : chanceobj.ssn(),
        "birthday" : chanceobj.birthday(),
        "name" : chanceobj.name(),
        "gender" : chanceobj.gender(),
        "hireDate" : chanceobj.date(),
        "address" : chanceobj.address(),
        "city" : chanceobj.city(),
        "state" : chanceobj.state(),
        "zip" : chanceobj.zip(),
        "homePhone" : chanceobj.phone(),
        "cellPhone" : chanceobj.phone(),
        "email" : chanceobj.email(),


    };
};

const people = Array.from({length :10000}, generatePerson);


require('fs').writeFile(

    './employeeRecords.txt',

    JSON.stringify(people),

    function (err) {
        if (err) {
            console.error('Something went wrong');
        }
    }
);

console.log(people);

