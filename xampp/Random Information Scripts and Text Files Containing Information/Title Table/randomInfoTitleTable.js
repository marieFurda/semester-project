const chance = require('chance');
const chanceobj = new chance();

const fs = require('fs') 

const generateTitle = () => {
    return {
        
        "from_Date" : chanceobj.date(),
        "to_Date" : chanceobj.date(),
        "department" : chanceobj.profession({rank:true})
        
    };
};

const title = Array.from({length :10000}, generateTitle);


require('fs').writeFile(

    './employeeTitle.txt',

    JSON.stringify(title),

    function (err) {
        if (err) {
            console.error('There is something wrong here');
        }
    }
);

console.log(title);
