const chance = require('chance');
const chanceobj = new chance();

const fs = require('fs') 

const generateRecord = () => {
    return {

        "department" : chanceobj.profession({rank:true}),
        "departmentNum" : chanceobj.integer({min:1, max:6})

    };
};

const dept = Array.from({length :10000}, generateRecord);


require('fs').writeFile(

    './departmentInfo.txt',

    JSON.stringify(dept),

    function (err) {
        if (err) {
            console.error('Something went wrong here');
        }
    }
);

console.log(dept);

