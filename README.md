**Requirements**

JavaScript v1.8.5

Node v14.15.1

Express v14.17.1

XAMPP v8.0.0


**Initial Steps in VS Code**

In order to begin this project you will first need to make a project folder file nodemysql
Cd into the file 

    cd CRUDapp

Create server and client folders and then cd into the server folder 

Npm init to make a initialize the folder and make a package.json file 

    npm init

Then you want to install the dependencies with: npm install express mysql body-parser 

    npm install express mysql body-parser

Then you want to install nodemon so that you don’t have to restart the server every time you save new changes: Npm install --save-dev nodemon

    npm install --save-dev nodemon

Open your project folder in visual studio code and in the client folder create an index.js file an index.html file and a stylesheet.css file

Input the code for each of these files which is provided

Also create a file entitled .env file that will contain information listed below :

PORT=5000
USER= (your username in phpMyAdmin)
PASSWORD= (your password that is assigned to your user on phpMyAdmin)
DATABASE=employees
DB_PORT=3306
HOST= (your host ip address seen on the general tab of xampp)

In order to initialize the app, all the while running the application index.js in nodemon and then navigate to localhost:5000 and you should see a cannot get/message because from here we are not getting any information from the app at this time, this is correct 

This concludes the basic set up of the app and connecting the app to the localhost web site 



**PHP and XAMPP set up** 

Download XAMPP version 8.0.0 at apachefriends.org and follow installation instructions 

Under the general tab, select start and then navigate to the services tab in order to see which of the servers are running. Wait until these are all running

Navigate the the volumes tab and select mount. That will create set up a remote server on your local computer 

Go back to the general tab and select open terminal and run the following code separately 

    Apt update

    Apt install vim 

    Vim /opt/lampp/etc/extra/httpd-xampp.conf

And change where it says require local to require all granted 

Restart the apache server and open phpMyAdmin 

Go back into the open terminal and cd into the /opt/lampp/phpmyadmin and type the following code separately

    Apt-get update 

    Apt-get install nano 

    Nano config.inc.php 

Next you want to change where it says 'config' and 'root' and change config it 'cookie' and root to '' and then sign into phpmyadmin using the 'root' as the username and nothing as the password 

Next navigate to the user accounts tab and for each of the root accounts, select them and change their passwords 

Log out of phpMyAdmin and log back in with the new password that you just established 

Next in the phpMyAdmin portal, you are going to navigate to database and select create a new database which will be named employee with rows each corresponding to an employee attribute from the randomly generated script

Add the respective attributes and each of their respective type values

Use the import function in phpMyAdmin in order to import the database in the text file to the employee database that you've just created



**Deploying the code provided**

Following this you are going to import the code that I provided into the respective files that you created on your machine through visual studio code

In the index.html file for this project right click and select open with live server in order to see the application deployed locally in your web browser, if this is not installed, navigate to the sidebar and select the extensions option. Type in live server and install.

Following this, I deployed the terminal within Visual studio code but you could also deploy the terminal locally on your machine 

Cd into the project folder from the terminal and and cd into the server folder 

    cd CRUDapp

Make sure your mySql and Apache servers are running in the xampp service and phpMyAdmin is deployed in your browser

Now in your terminal, run your program using the command:
Nodemon app.js 

Navigate to the browser window that you opened with Visual Studio Code live server 



